const options = {

    methods: {
        addNote(){
            if (!this.newNote.text) {
                return;
            }
            var text = this.newNote.text
            text = text.charAt(0).toUpperCase() + text.slice(1);
            this.newNote.id = Date.now()
            this.newNote.title = text.substr(0,29)+"..."
            this.newNote.text = text
            this.newNote.length = text.split(' ').length
            this.newNote.visible = 'false'
            this.newNote.edit = 'false'
            this.notes.push(this.newNote);
            try {
                localStorage.setItem("savedNotes", JSON.stringify(this.notes));
            } catch (e) {
            }
            this.newNote = {}
            
            console.log(this.notes)
        },
        removeNote(note){
            const index = this.notes.indexOf(note);
            if (index > -1) {
                this.notes.splice(index, 1);
            }
            try {
                localStorage.setItem("savedNotes", JSON.stringify(this.notes));
            } catch (e) {
            }
        },
        updateNote(note) {

            const index = this.notes.indexOf(note);
            if (index > -1) {
                var titleNote = note.text
                note.title = titleNote.substr(0,29)+"..."
                note.length = titleNote.split(' ').length
            }
            try {
                localStorage.setItem("savedNotes", JSON.stringify(this.notes));
            } catch (e) {
            }
            titleNote=""
        },
    },
    data : function () {
        return {
            note: {id: "", title:"", text: "", edit:"", length:"", visible:""},
            newNote : {id : "", title:"", text : "", edit:"", length:"", visible:""},
            notes : []
        }
    },
    mounted: function () {
        const savedNotes = JSON.parse(localStorage.getItem("savedNotes"));
        if (savedNotes) {
            return this.notes = savedNotes;
    }
    }
};
Vue.createApp(options).mount("#app");
